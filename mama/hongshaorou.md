# Mama's Hongshaorou 
SHANGHAI-STYLE BRAISED PORK BELLY

## Ingredients 
- 3 /4 lb. of lean pork belly (cut into 3/4-inch thick pieces)
- 2 tablespoons olive oil
- 3 tablespoons shaoxing wine or mirin 
- 1 tablespoon regular soy sauce
- ½ tablespoon dark soy sauce
- ½ teaspoon Kosher Salt
- a handfull of rock sugars 
- 1 tablesoon brown sugar
- one giner 

## Preparation 
1. Wash pork belly, peel ginger and cut into slices.
2. Cut pork belly into blocks of mahjong size.
3. Boil pot of hot water with 2 slices of giner, when boiling, soak pork in until fat foams comes out. Wash out fat foams with hot (important) tap water. 
4. Over low heat, add oil and ginger to your wok. When ginger is hot, raise the heat to medium and cook the pork until pork is lightly browned.
5. Soak cooked pork belly in water, cover and simmer for about 45 minutes in low heat. Every 5-10 minutes, stir to prevent burning and add more water (hot) if it gets too dry. 
6. Once the pork is fork tender, when there is still a lot of visible water, uncover the wok, turn up the heat, add soy sauce, cooking wine, dark soy source, brown suger, rock sugar, some salk to the sauce. 
7. Stir continuously until the sauce has reduced to a glistening coating. Make sure the pork blocks are properly caramelize. 
8. [Optional] You can add cooked potation into the wok before pork is ready. Usually I would steam cook potatos separately and add it into the wok 5 mins before the dish is ready. 