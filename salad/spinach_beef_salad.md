# Spinach Salad  



## Base 
- Spinach baby leaf

## Vegetables & Fruit
- Cherry tomatoes
- Pepper 
- Asparagus 

## Protein 
- Roasted beef (sliced)
or 
- Spanish Jambon 
- Sun flower seeds 

## Toppings 
- Blue cheese or Pamesan 
- Raisins 

## Dressing & Spices 
- Balsamic vinegar
- Olive oil
- Red wine vinegar 


 