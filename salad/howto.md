# The Elements of A Salad 

## Base 

Usually makes a big part of a salad (~50%), leafy greens or pasta, grains, potatoes (not a fan) or even noodles. Shaved carrots or zucchini ribbons can also be a good option for base.  
- cauliflower
- cucumber
- asparagus 
- kale
- cabbige 
- baby spinache 

## Vegetables & Fruit

25% of the salad, vegies fresh and raw, or roasted, steamed, boiled. Preserved vegetables in olive oil is also great option.   
### Good to go raw 
- cucumbers
- peppers
- tomatoes 
- red onions 
- carrots
- celery
- zucchini 
- radishes
- beets 
- cauliflower 
- broccoli
- fennel
- parsnip
- asparagus
- green beans 
- shallots 

### Good to steam 

- abc 
- abc 

### Good to roast 
- abc
- abc 

## Protein 

Usually you can pick two types of protein, such as a nut and a cheese, a seed and a fish, a cheese and some meat. 
- meat (chicken, duck, goose, beef, turkey, pork) 
- seafood (cod, tuna, mackerel, shrimp, salmon, sardines, scallops, sea bass, trout, squid, octopus)
- eggs
- cheese
- soy product (tofu)
- grains (quino, wheat, couscous, rice, rye, oats, buckwheat)
- nuts and seeds (hemp, almonds, hazelnuts, walnuts, cashews, pine nuts, pistachios, pumpkin seeds, seasame seeds) 


## Toppings 
- Blue cheese or Pamesan 
- Raisins 

## Dressing & Spices 
- Balsamic vinegar
- Olive oil
- Red wine vinegar 


 